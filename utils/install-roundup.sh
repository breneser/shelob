#!/usr/bin/env bash
set -e

wget https://github.com/bmizerany/roundup/tarball/v0.0.6 -O roundup.tar.gz
tar -xzvf roundup.tar.gz
mv bmizerany-roundup* roundup
cd roundup || exit 1
./configure
make
make install
