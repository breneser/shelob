#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

make

git release --semver "${1:-}"
