# Shelob

A collection of utilities for Bash 3.0+

<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [Shelob](#shelob)
    - [Modules](#modules)
        - [Colors](#colors)
            - [Environment variables for colors](#environment-variables-for-colors)
        - [Logger](#logger)
            - [Environment variables for logger](#environment-variables-for-logger)
        - [String](#string)
        - [Filesystem](#filesystem)
        - [Interactive](#interactive)
            - [Environment variables for interactive](#environment-variables-for-interactive)
    - [Installation](#installation)
    - [References](#references)

<!-- markdown-toc end -->


## Modules

### Colors
[Source colors<span>.</span>sh](#installation) to use available color functions:


```bash
#!/usr/bin/env bash
echo "Hello" | red
echo "Hello" | red underline
echo "Hello" | black green_bg bold
```

will produce. 

![colors](docs/images/colors.png)

All formatting functions can be chained as above.

Available formatting options are as follows:

**foreground colors**
- black
- red
- green
- yellow
- blue
- magenta
- cyan
- white

**backgroundcolors**
- black_bg
- red_bg
- green_bg
- yellow_bg
- blue_bg
- magenta_bg
- cyan_bg
- white_bg

**styles**
- bold
- underline


Colors are printed automatically depending on current output. If current output is a tty the functions will output the colors.

Redirection and further piping will disable the colors. The following will not output any colors.

```bash
 printf "%s" "This will not output any colors" | blue > a.out
```

#### Environment variables for colors
| Variable      | Options             | Description |
| ---------     | -------             | ----------- |
| SHELOB_COLOR  | auto\|always\|never | Default is `auto`. To enable colors even when output is not tty set value to `always`. Set `never` to disable |


### Logger


Logging supports colors and is compatible with [Syslog Severity levels](https://en.wikipedia.org/wiki/Syslog#Severity_levels).

[Source logger<span>.</span>sh](#installation) to use available logging functions:

```bash
#!/usr/bin/env bash

source ${SHELOB_HOME}/logger.sh

SHELOB_LOG_LEVEL=7
debug "Debug message" #SHELOB_LOG_LEVEL=7
info "Info message" #SHELOB_LOG_LEVEL=6
notice "Notice message" #SHELOB_LOG_LEVEL=5
warning "Warning message" #SHELOB_LOG_LEVEL=4
error "Error message" #SHELOB_LOG_LEVEL=3
critical  "Critical message" #SHELOB_LOG_LEVEL=2
emergency "Emergency message" #SHELOB_LOG_LEVEL=1
```

will output:

![Log messages](docs/images/logs.png)

Colors are disabled if output is not tty.

Be careful when using emergency as it will exit the program with the exit value 1

#### Environment variables for logger

| Variable              | Options       | Description      |
| -----                 | ----------    | ---------        |
| SHELOB_LOG_TIMESTAMP  | true\|false    | Default=false. Set `true` to output timestamp with log messages. |
| SHELOB_COLOR  | auto\|always\|never | Default is `auto`. To enable colors even when output is not tty set value to `always`. Set `never` to disable. Note that this will affect [ color ](#colors) functions as well. |
| SHELOB_LOG_LEVEL      | 1\|2\|3\|4\|5\|6\|7              | Default is 6 (only disables debug messages). When log level is set to a number, all logs at smaller levels will also be enabled. e.g. to only see `error` level messages and messages at lower level (critical, emergency) set SHELOB_LOG_LEVEL to 3 |

### String


[Source string<span>.</span>sh](#installation) to use string functions. 

Available string functions are as follows:

- `is_empty` : test if a string is empty(blank, all spaces)
  
  ```bash
  if is_empty "$text"; then
    echo "String is empty"
  else
    echo "String is NOT empty"
  fi
  ```

- `not_empty` : test if a string is NOT empty(blank, all spaces)
  
  ```bash
  if not_empty "$text"; then
    echo "String is NOT empty"
  else
    echo "String is empty"
  fi

- `ltrim` : trim leading spaces and newline characters

  ```bash
  echo "$(ltrim " Hello   ")" #will output "Hello   " without quotes
  ```

- `rtrim` : trim trailing spaces and newline characters

  ```bash
  echo "$(rtrim "  Hello   ")" #will output "  Hello" without quotes
  ```


- `trim` : trim surrounding spaces and newline characters
  ```bash
  echo "$(trim "   Hello   ")" #will output "Hello" without quotes
  ```

### Filesystem

[Source filesystem<span>.</span>sh](#installation) to use file system functions.

Available functions and example usages (in file.sh example file) are as follows:

```bash
#!/usr/bin/env bash
#/home/bren/file.sh
source "$SHELOB_HOME/filesystem.sh"

#All three functions below will resolve links and output actual file paths

dir=$(source_dir) # To get canonical directory path of current file "/home/bren"
path_=$(source_path) # To get canonical file path  "/home/bren/file.sh"
base_=$(source_file_name) # To get base file name  "file.sh"

echo "Source path is $dir"
echo "File path is $file"
echo "File name is $base"
```

Running file<span>.</span>sh will produce following output:

```bash
> /home/bren/file.sh

Source path /home/bren
File path is /home/bren/file.sh
File name is file.sh
```

### Interactive

A collection of functions to get input from user interactively. 

[Source shelob-interactive<span>.</span>sh](#installation) to use available functions:

Check out following example:

```bash
#!/usr/bin/env bash

source lib/shelob-interactive.sh

#Default valie is passed with -d or --default option and must be first argument.
#'name' is the variable the input will be assigned to
#If user hits enter it will use default value if provided otherwise empty
#reponse is accepted
ask_input --default "John Doe" name "What is your name?" 

# Ask a yes no question to user.The function is meant to be used with
# conditional statements.
# Default value is passed with -d or --default option and must be firest argument.
# Accepted values are Y y n N, any other value will set default value as Yes
# and print a warning message denoting this.
if ask_yes_no --default "n" "Hello $name, would you like some coffee?"; then
  # Ask user to chose an option.
  # Default value is passed with -d or --default option and must be firest argument.
  # 'answer' is the variable the answer will be assigned to.
  # following arguments are options to be displayed to user.
  ask_option -d 1  answer "How would you like your coffee?" "black" "white"
  echo "$answer coffee coming right up"
else
  # Same as ask_input, except input is required.If no default value is provided
  # and user does not input anything it will ask again
  ask_input_required reason "Okay then, why not?"
  echo "Really? \"$reason\" is not a valid reason to turn down a cup of coffee"

fi
```

will produce following output:

![interactive output](docs/images/interactive.png)

#### Environment variables for interactive

| Variable                  | Options        | Description        |
| ------------------------- | -------------- | ------------------ |
| SHELOB_ANSWER_ALL         | true\|false     | Default is false. Set to true if it is meant to be used in a non-interactive manner. If set to true user will not be prompted and default answers will be used.Useful for non-interactive scripts. |

## Installation

To install globally clone and run install script as root. You need to login again to have SHELOB_HOME environment variable available for use.

```bash
git clone https://gitlab.com/breneser/shelob.git
cd shelob
sudo ./install.sh
```

To install for a single user you need to specify a PREFIX for Shelob to be installed under:

```bash
git clone https://gitlab.com/breneser/shelob.git
cd shelob
PREFIX=/home/bren/lib ./install.sh
```

If you install without root privileges you will need to export SHELOB_HOME environment variable in your shell startup file. 

Installation will show you the exact line to be added to your shell start file.



## References

All thanks to following.I cobbled together scripts and used ideas from these awesome sources and projects:

- [Bash package panager](https://github.com/bpkg/bpkg/)
- [Bash3 Boilerplate](http://bash3boilerplate.sh/)
- [Shell Script Loader](https://loader.sourceforge.io)
- [Pearl](https://github.com/pearl-core/pearl)
- And of course folks contributing to [Stackoverflow](https://stackoverflow.com/)
