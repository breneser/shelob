
0.5.3 / 2018-09-25
==================

  * Remove release hook function

0.5.2 / 2018-08-08
==================

  * Remove tee dependency

0.4.2 / 2018-08-07
==================

  * Add gitlab ci jobs

0.3.2 / 2018-08-07
==================

  * Fix test sources invalid files
  * Fix global installation variable not exported
  * Refactor documentation

0.2.2 / 2018-08-04
==================

  * Add install/uninstall scripts

0.1.2 / 2018-08-04
==================

  * Add function_exists test function

0.0.2 / 2018-08-03
==================

  * Make version update more verbose

0.0.1 / 2018-08-03
==================

  * Add release script
  * Add CHANGELOG
