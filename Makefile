all: test

test:
	tests/teststyle.sh
	SHELOB_HOME="$(shell pwd)/src" roundup tests/unit-tests/*

install:
	utils/install.sh
