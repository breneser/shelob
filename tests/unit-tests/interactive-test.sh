#!/usr/bin/env bash
# shellcheck disable=2154,2034,1117

describe "Interactive"

source src/interactive.sh

it_uses_default_yes_no_choice() {
  ! ask_yes_no --default "n" "Hello" <<< ""
  ask_yes_no -d "y" "Hello" <<< ""
}

it_returns_success_if_answered_yes() {
  ask_yes_no -d "n" "Hello" <<< "Y"
  ask_yes_no --default "n" "Hello"  <<< "y"
}

it_returns_failure_if_answered_no() {
  ! ask_yes_no -d "Y" "Hello" <<< "N"
  ! ask_yes_no -d "Y" "Hello" <<< "n"
}


it_asks_again_if_no_default_answer_given() {
  local output
  output=$(echo -e "\ny" | ask_yes_no "Hello")
  echo "$output"
  [[ $output =~ Hello.*Hello ]]
}

it_asks_again_if_answer_is_invalid() {
  local output
  set +e
  set +x
  output=$(echo -e "Hi\nn" | ask_yes_no -d "n" "Hello")
  test $? -ne 0
  set -x
  [[ $output =~ Hello.*Hello ]]
}

it_uses_default_input_if_no_input_given() {
  ask_input -d "Hi" "testvar" "Hello" <<< ""
  test "Hi" = "$testvar"
}

it_uses_given_input() {
  ask_input --default "Hi" "testvar" "Hello"  <<< "Howdy"
  test "Howdy" = "$testvar"
}

it_works_without_default_input() {
  ask_input testvar "Hello" <<< ""
  test "" = "$testvar"
}

it_fails_if_no_variable_name_given() {
  ! ask_input -d "Hey" <<< ""
}

it_asks_again_if_input_required_and_empty() {
  local output
  output=$(echo -e "\nHowdy" | ask_input_required testvar "Hello")
  echo "$output"
  [[ $output =~ Hello.*Hello ]]
  ask_input_required testvar "Hello" <<< "$(echo -e "\nHowdy")"
  test "Howdy" = "$testvar"
}

it_uses_default_option_if_not_chosen() {
  SHELOB_LOG_LEVEL=7
  ask_option --default 2 testvar "Which one?" "a" "b" <<< ""
  test "b" = "$testvar"
}
