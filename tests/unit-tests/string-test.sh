#!/usr/bin/env bash
# shellcheck disable=1117


describe "String"

source src/string.sh

it_reports_all_blanks_as_empty() {
    is_empty "  "
}

it_reports_non_empty_string_correctly() {
  ! is_empty "Test "
}

it_reports_no_argument_as_empty() {
  is_empty
}

it_reports_all_blanks_as_empty_with_negative_check() {
  ! not_empty "  "
}

it_reports_non_empty_string_correctly_with_negative_check() {
    not_empty "Test "
}

it_reports_no_argument_as_empty_with_negative_check() {
  ! not_empty
}


it_trims_right_side() {
  local trimmed=
  trimmed="$(rtrim " Hello    ")"
  test " Hello" = "$trimmed"
}

it_trims_right_side_with_new_line() {
  local trimmed=
  trimmed="$(rtrim "$(echo -e " Hello\n    ")")"
  test " Hello" = "$trimmed"
}

it_trims_left_side() {
  local trimmed=
  trimmed="$(ltrim " Hello    ")"
  test "Hello    " = "$trimmed"
}

it_trims_left_side_with_new_line() {
  local trimmed=
  trimmed="$(ltrim "$(echo -e "\n \nHello\n    ")")"
  test "$(echo -en "Hello\n    ")" = "$trimmed"
}

it_trims_both_sides() {
  local trimmed=
  trimmed="$(trim " Hello    ")"
  test "Hello" = "$trimmed"
}

it_trims_both_side_with_new_line() {
  local trimmed=
  trimmed="$(trim "$(echo -e "\n \nHello\n    ")")"
  test "Hello" = "$trimmed"
}
