#!/usr/bin/env bash
#shellcheck disable=2154,2034,2119

describe "Colors"

before() {
  export TERM='xterm'
  source src/colors.sh
  SHELOB_COLOR='always'
}

it_formats_when_terminal_is_connected() {
  local output
  output="$(printf "test" | black)"
  test "$output" != "test"
  test "${__black}test${__reset}" = "$output"
  output="$(printf "test" | red)"
  test "${__red}test${__reset}" = "$output"
  output="$(printf "test" | green)"
  test "${__green}test${__reset}" =  "$output"
  output="$(printf "test" | yellow)"
  test "${__yellow}test${__reset}" = "$output"
  output="$(printf "test" | blue)"
  test "${__blue}test${__reset}" = "$output"
  output="$(printf "test" | magenta)"
  test "${__magenta}test${__reset}" = "$output"
  output="$(printf "test" | cyan)"
  test "${__cyan}test${__reset}" = "$output"
  output="$(printf "test" | white)"
  test "${__white}test${__reset}" = "$output"
  output="$(printf "test" | black_bg)"
  test "${__black_bg}test${__reset}" = "$output"
  output="$(printf "test" | green_bg)"
  test "${__green_bg}test${__reset}" = "$output"
  output="$(printf "test" | yellow_bg)"
  test "${__yellow_bg}test${__reset}" = "$output"
  output="$(printf "test" | blue_bg)"
  test "${__blue_bg}test${__reset}" = "$output"
  output="$(printf "test" | magenta_bg)"
  test "${__magenta_bg}test${__reset}" = "$output"
  output="$(printf "test" | cyan_bg)"
  test "${__cyan_bg}test${__reset}" = "$output"
  output="$(printf "test" | white_bg)"
  test "${__white_bg}test${__reset}" = "$output"
  output="$(printf "test" | bold)"
  test "${__bold}test${__reset}" = "$output"
  output="$(printf "test" | underline)"
  test "${__underline}test${__reset}" = "$output"
}

it_formats_with_multiple_formats() {

  local output
  output="$(printf "test" | black underline)"
  test "${__black}${__underline}test${__reset}" = "$output"
  output="$(printf "test" | underline red)"
  test "${__underline}${__red}test${__reset}" = "$output"
  output="$(printf "test" | red bold underline)"
  test "${__red}${__bold}${__underline}test${__reset}" = "$output"
}

it_formats_in_given_order() {
  local output
  output="$(printf "test" | underline black bold)"
  test "${__underline}${__black}${__bold}test${__reset}" = "$output"
  test "${__black}${__bold}${__underline}test${__reset}" != "$output"
}

function __multiple_lines() {
  cat << EOF
  Multiple
  Lines
EOF
}

it_formats_multiple_lines() {
  local output
  local outputsum
  local expectedsum
  local text

  text="$(__multiple_lines)"

  output="$(printf "%s" "$text" | underline black bold)"

  expectedsum="$(printf "%s" "${__underline}${__black}${__bold}$text${__reset}" | md5sum )"
  outputsum="$(printf "%s" "$output" | md5sum )"

  [[ $outputsum = "$expectedsum" ]]

}

it_does_not_format_when_output_is_not_tty() {
  SHELOB_COLOR='auto'
  # test in a subshell
  output="$(printf "test" | red)"; \
  [[ "$output" = "test" ]]
}
