#!/usr/bin/env bash

source src/filesystem.sh
status=0

testdir="$(pwd)/tests"
expected_dir="$testdir/utils"
expected_path="$testdir/utils/sourcefilesystem.sh"
expected_file_name='sourcefilesystem.sh'
path="$(source_path)"
dir="$(source_dir)"
file_name="$(source_file_name)"

echo "Dir: $dir , Exptected: $expected_dir"
echo "File Name: $file_name , Exptected: $expected_file_name"
echo "Path: $path, Expected: $expected_path"
[[ $dir = "$expected_dir" ]] || status=1
[[ $file_name = "$expected_file_name" ]] || status=1
[[ $path = "$expected_path" ]] || status=1

# if script is being executed rather than sourced exit with status
if [[ ! ${BASH_SOURCE[0]} != "$0" ]]; then
  exit $status
fi
